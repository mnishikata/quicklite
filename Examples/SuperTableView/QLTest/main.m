//
//  main.m
//  QLTest
//
//  Created by Masatoshi Nishikata on 20/08/13.
//
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
	return NSApplicationMain(argc, argv);
}
