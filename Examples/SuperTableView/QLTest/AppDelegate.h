//
//  AppDelegate.h
//  QLTest
//
//  Created by Masatoshi Nishikata on 20/08/13.
//
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
